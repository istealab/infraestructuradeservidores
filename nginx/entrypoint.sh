#!/bin/bash

# si el script produce algun error, que se detenga su ejecucion
set -e

# Instalar nginx
apt update && apt install nginx -y
cp /Default /etc/nginx/sites-available
chown -R www-data:www-data /var/www

# Descargamos la app desde un repositorio
wget -O /var/www/index.html https://pastebin.com/raw/e88cYYag

# La evaluacion del condicional se basa en el exit status del comando 'ls' si el fichero no existe, es necesario que la condicional devuelva 'true'
# para ello usamos el operador '!' (not) si no existe el fichero 'index.html' entonces copialo desde el directorio raiz.
if ! ls /var/www/index.html; then
cp /index.html /var/www
fi

#'$@' representa arugmentos pasados al script, el fichero entrypoint.sh finaliza con 'exec "$@"' por que a ese fichero se la van a pasar las intrucciones que vengan de 'CMD'
exec "$@"
