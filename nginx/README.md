**Nombre:** Federico Martin Casco
**Fecha:** 7/12/2021
**Materia:** Infraestructura de Servidores

## Descripción

Se instalará un contenedor que ejecute nginx como servidor web a partir de una imagen creada de Ubuntu.

## Requisitos
Se utilizó en VirtualBox una VM Linux con las siguientes características:
-Linux Ubuntu 18.04.6 LTS
-Linux Additionls tools instladas
-Placa de red configurada en modo puente
-Instalación de SSH, para conexión remota
-Tener instalado Git
-Contar con SSH Key en Gitlab para subir el repositorio

## Instrucciones
###Creamos la carpeta y verificamos el directorio donde crearemos el Script de instalación de Docker
root@fedex:/home/fedex# mkdir nginx
root@fedex:/home/fedex# cd nginx
root@fedex:pwd
home fedex nginx

###Creamos el archivo xxx.sh y cipiamos el script dentro. Esto instalara todo lo necesario para que Docker funcione.
root@fedex:nano home/fedex/nxinx/InstallDocker.sh
-------------------------------------
#!/bin/bash
 
if [ $UID -ne 0 ]; then
    echo "ejecute 'sudo $0'"
 fi

#Actualiza el sistema y Instale los certificados para trabajar con https 
apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common

#Añada la clave GPG oficial de Docker
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

#Añada el repositorio correspondiente a su versión de Ubuntu 
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#Actualiza el sistema
apt update

#Instala la última versión de Docker 
apt install docker-ce docker-ce-cli containerd.io

--------------------------------
###Guardar cambios y salir
###Dar permisos de ejecución al script y ejecutarlo
root@fedex:chmod +x InstallDocker.sh
root@fedex:./InstallDocker.sh

### Comprobar que docker se intalo correctamente y esta actiivo el servicio.
root@fedex:systemctl status docker

### Ya tenemos el sistema actualizado y Docker instalado, a continuación crearemos el Dockerfile que usaremos para crear la imagen Docker 
root@fedex:/home/fedex# nano Dockerfile

Agregamos la siguiente información:
------------------------------------------
FROM ubuntu:20.04 
LABEL version="0.1"
LABEL description="Esta es una imagen de prueba nginx"
ARG DEBIAN_FRONTEND=noninteractive
COPY default /etc/nginx/sites-available
COPY entrypoint.sh /
COPY index.html
RUN chmod +x /entrypoint.sh
VOLUME ["/var/www"]
EXPOSE 80 443
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
------------------------------------------------------
Dónde:
FROM: nos indica una imagen base para crear una nueva imagen (ubuntu:20.04) 
LABEL: nos indica información acerca de la imagen a crear, en este caso la versión y una descripción
ARG DEBIAN_FRONTEND: la instrucción setea variables de entorno dentro del contenedor que se creara para crear la imagen y la variable 'DEBIAN_FRONTEND' le pasa el tipo de ejecución a los programas que lo soliciten.
COPY: copia ficheros hacia el contendor temporal. En el primer caso un Default con la configuración del nginx; en segundo el script para instalar el nginx y el último un index.html por defecto.
RUN: ejecuta un comando dentro del contenedor temporal (chmod +x /entrypoint.sh)
VOLUME: habilita los directorios que pueden ser montados con directorios externos (["/var/www"])
EXPOSE: los puertos que estarán habilitados (80 443)
ENTRYPOINT: permite configurar un contenedor que se ejecutará como ejecutable, este caso el argumento es script que instalara nginx ["/entrypoint.sh"]
CMD: indica los comando que se ejecutaran cuando el contenedor se inicie (["nginx", "-g", "daemon off;"])

### Antes de avanzar veamos el script entrypoint.sh
-------------------------------------------------
#!/bin/bash
# Si el script produce algún error, que se detenga su ejecución
set -e
# Instalar nginx
apt update && apt install nginx -y
cp /default /etc/nginx/sites-available
chown -R www-data:www-data /var/www
# Descargamos la app desde un repositorio
wget -O /var/www/index.html https://pastebin.com/raw/e88cYYag
# La evaluación del condicional se basa en el exit status del comando 'ls' si el fichero no existe, es necesario que la condicional devuelva 'true'. Para ello usamos el operador '!' (not) si no existe el fichero 'index.html' entonces copialo desde el directorio raiz.
if ! ls /var/www/index.html; then
cp /index.html /var/www
fi
#'$@' representa arugmentos pasados al script, entrypoint.sh finaliza con 'exec "$@"' por que se la van a pasar las intrucciones que vengan de 'CMD'
exec "$@"
-----------------------------------------------------------------------------------

### Ahora tenemos todos los componentes para levantar el contenedor nginx, antes generaremos la nueva imagen con el Dockerfile. Para construir el contenedor usamos docker build con el parámetro  -t para indicar el nombre de la imagen y finalizamos con (.) que indica el nombre del archivo Dockerfile (en este caso al ser por defecto ponemos .)
root@fedex:/home/fedex/nginx# docker build -t fedex/nginx .

### Para crear un contenedor a partir de la nueva imagen utilizamos docker run con el nombre del contenedor y el nombre de la imagen.
root@fedex:/home/fedex/nginx# $ docker run -d --name=nginx.infra.istea fedex/nginx

### Ya se puede comprobar el funcionamiento del contenedor por http en la IP del servidor.

### Subir repositorio a GitLab. Para clonar el repositorio, utilizamos el comando git clone (con el parametro --Whit SSH Clone obtenido de Gitlab), luego lo agregamos, realizamos el commit ("Descripcion") y finalmente el push 
root@fedex:git clone git@gitlab.com:istealab/infraestructuradeservidores.git
root@fedex:~/infraestructuradeservidores# cp -r /home/fedex/nginx /root/infraestructuradeservidores/
root@fedex:~/infraestructuradeservidores# git config --global user.email "federicomartin.casco@gmail.com"
root@fedex:~/infraestructuradeservidores# git commit -m "Update Infra"
root@fedex:~/infraestructuradeservidores# git add .
root@fedex:~/infraestructuradeservidores# git commit -m "Update Infra"
root@fedex:~/infraestructuradeservidores# git push origin master


## Fuentes
https://docs.docker.com/docker-hub/repos/
https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-docker/
https://docs.gitlab.com/ee/
