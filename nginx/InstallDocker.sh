#!/bin/bash
 
if [ $UID -ne 0 ]; then
    echo "ejecute 'sudo $0'"
 
fi
 
apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
 
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
 
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
 
apt update
 
apt install docker-ce docker-ce-cli containerd.io
